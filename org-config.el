;;; org-config.el --- A configuration manager on org-mode

;; Copyright (C) 2022  Alejandro Barocio A.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Author: Alejandro Barocio A. <abarocio80@gmail.com>
;; Version: 0.1
;; Package-Requires: ((org))
;; Keywords: org-mode, configuration, autotangle, autocompile
;; URL: https://gitlab.com/abarocio80/org-config.el

;;; Commentary:

;; Most of this code in inspired by video of David Wilson on his
;; YouTube Channel (System Crafters) entitled "Emacs From Scratch #7 -
;; Configure Everything with Org Babel"
;; (https://www.youtube.com/watch?v=kkqVTDbfYp4&t=1383s).

;; This package is intended to have your configuration files (emacs or
;; else) organized in org-documents under a single directory tree.
;; All code blocks will be automatically tangled on saving the file,
;; then, it will try to compile all emacs-lisp generated files
;; (bytecode or native), unless those options are deactivated.

;; It will also have a couple of commands to interact directly to the
;; user via the emacs UI.  One command is to visit a file under your
;; configuration directory, named `org-config-visit-file'; the other
;; one is to tangle a file under your configuration directory, named
;; `org-config-tangle-file'.

;;; Code

;;; Dependencies
(require 'ob)				; Uses org-babel

;;; Variables

(defvar org-config-directory (expand-file-name "config" org-directory)
  "Path to the directory containing your org-config files.")

(defvar org-config-autotangle t
  "Whether to autotangle org-config files.")

(defvar org-config-el-autocompile t
  "Whether to autocompile emacs-lisp (.el) generated files.")

(defvar org-config-recurse-directory t
  "Whether to recurse over subdirectories of `org-config-dir'.")

(defvar org-config-prompt-string "Config file"
  "The prompt to display when asking for a file under `org-config-directory'.")

(defvar org-config-tangle-string "Tangle"
  "The string to display on prompt for the tangle action.")

(defvar org-config-find-string "Find"
  "The string to display on prompt for the find action.")

;;; Functions

(defun org-config--empty-string-as-nil (s)
  "Returns nil if the string is empty.
Otherwise, returns S. "
  (when (or s (string-empty-p s))
    nil s))

(defun org-config--list-tangled-files (orgfile &optional ext)
  "Returns a list of files to be extracted from ORGFILE."
  (let ((files-list '())
        (x-buffers-list (buffer-list)))
    (with-current-buffer (get-file-buffer (expand-file-name orgfile))
      (save-excursion
        (goto-char (point-min))
        (while (search-forward-regexp "\s:tangle\s+" nil t)
          (let ((out-file
                 (or (thing-at-point 'filename t)
                     (eval (sexp-at-point)))))
            (when (and (org-config--empty-string-as-nil out-file)
                       (not (string-equal out-file "no")))
              (if (org-config--empty-string-as-nil ext)
                  (when (string-suffix-p ext out-file)
                    (add-to-list 'files-list out-file t))
                (add-to-list 'files-list out-file t))))
          (when (not (member (current-buffer) x-buffers-list))
            (kill-buffer (current-buffer))))))
    files-list))

;; (org-config--list-tangled-files "~/org/config/emacs/config.org")

(defun org-config-tangle-orgfile (orgfile)
  "Tangles the ORGFILE with no evaluation confirmation."
  (let ((org-babel-confirm-evaluate nil)
        (files (org-babel-tangle-file orgfile)))
    (message "Files of tangle process: %s" files)
    files))

(defun org-config-compile-el-files (files)
  "Compiles the emacs-lisp files (*.el) referenced in ORGFILE.
When available, it will native-compile the files, otherwise it
will bytecode-compile them."
  (dolist (file files)
      (message "Checking if have to compile file %s" file)
      (when (and (file-exists-p file)
                 (string-suffix-p ".el" file))
        (message "Compiling file %s..." file)
        (delete-file (format "%sc" file))
        (delete-file (format "%sn" file))
        (if (fboundp 'native-compile-async)
            (native-compile-async file (format "%sn" file))
          (byte-recompile-file file nil t)))))

(defun org-config-compile-all-el ()
  (interactive)
  (if (boundp 'user-config-directory)
      (native-compile-async (or user-config-directory
                                (expand-file-name "config" user-emacs-directory))
                            t nil "\.el$"))
  (native-compile-async (expand-file-name "config" user-emacs-directory)
                        t nil "\.el$"))

;;;:::autoload
(defun org-config-tangle-and-compile-maybe (&optional orgfile)
  "Tries to tangle and compile current buffer.

First, if `org-config-autotangle' is non-nil, it will tangle the
content; then it will try to compile the resulting emacs-lisp
files, but only if `org-config-el-autocompile' is non-nil.

If optional argument ORGFILE is non-nil, it will try to do the
previous process with that file."
  (let ((file (or orgfile (buffer-file-name))))
    (when (string-prefix-p (expand-file-name org-config-directory)
                           (expand-file-name file))
      (when org-config-autotangle
        (if org-config-el-autocompile
            (org-config-compile-el-files (org-config-tangle-orgfile file))
          (org-config-tangle-orgfile file))))))

(defun org-config--files-list ()
  "Returns the list of the org files under `org-config-directory'.
It ignores all special files, like autosaves and backups."
  (if org-config-recurse-directory
      (directory-files-recursively (expand-file-name org-config-directory) "\\.org$")
    (directory-files (expand-file-name org-config-directory) t "\\.org$")))

(defun org-config--file-prompt (action)
  "Prompts for and returns a file name under `org-config-directory'."
  (expand-file-name (read-file-name (format "%s [%s]: " org-config-prompt-string action)
				   (file-name-as-directory org-config-directory)
				   (file-name-as-directory org-config-directory)
				   t)))

;;;:::autoload
(defun org-config-tangle-file (&optional orgfile)
  "Selects and tangle a file from the directory `org-config-directory'."
  (interactive)
  (org-config-tangle-and-compile-maybe
   (or orgfile
       (org-config--file-prompt org-config-tangle-string))))

;;;:::autoload
(defun org-config-visit-file (&optional orgfile)
  "Visits a file named ORGFILE from the directory `org-config-directory'.

Interactively prompts for a file under `org-config-directory'.

When ORGFILE is non-nil it will not ask for a file
interactivelly, and instead use the value of ORGFILE."
  (interactive)
  (find-file
   (or orgfile
       (org-config--file-prompt org-config-find-string))))

(provide 'org-config)
;; (insert (buffer-file-name))
